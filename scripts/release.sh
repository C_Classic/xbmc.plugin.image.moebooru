#!/bin/bash

# Config
bp=..
rp=../release
plugin=plugin.image.moebooru
log=/tmp/$plugin.log

# Functions begin here
addStep() {
	echo -ne "[ ] $1"
}
tickStep() {
	echo -e "\r[\u2713"
}
failStep() {
	echo -e "\r[\u00D7"
}
checkApp() {
	addStep "Checking application $1..."
	$1 --help 2&> /dev/null
	if [[ $? != 127 ]]; then
		tickStep
		return 0
	else
		failStep
		return 1
	fi
}


# Code begins here
echo -e "\n\t# Building $plugin #\n"
echo "" > $log

# Sanity check
echo "Checking whether build environment is sane..."
	failed=0
	for app in rm cp mkdir find convert zip
	do
		checkApp $app
		if [[ $? != 0 ]]; then
			failed=1
		fi
	done

	if [[ $failed != 0 ]]; then
		echo "ERROR: Not all required programs have been found!"
		echo "       Check the list above to find the missing ones."
		exit 1
	fi
	
	if [ -d $rp ]; then
		echo "ERROR: Another build already exists!"
		echo "       Please delete that one first."
		exit 2
	fi
	
echo "Starting build..."
# Initial setup
addStep "Copying all plugin files to the release path..."
	failed=0
	mkdir -p $rp/$plugin 2>&1 >> $log
	if [[ $? != 0 ]]; then
		failed=1
	fi
	cp -ra $bp/$plugin $rp/ 2>&1 >> $log
	if [[ $? != 0 ]]; then
		failed=1
	fi
	if [[ $failed != 0 ]]; then
		failStep
		echo "ERROR: Could not copy files!"
		echo "       Check error log under $log."
		exit 1
	else
		tickStep
	fi

addStep "Removing xml language files..."
	failed=0
	xmls=`find $rp/$plugin/resources/language -name *.xml 2>> $log`
	if [[ $? != 0 ]]; then
		failed=1
	fi
	for xml in $xmls; do
		rm $xml 2>&1 >> $log
		if [[ $? != 0 ]]; then
			failed=1
		fi
	done
	if [[ $failed != 0 ]]; then
		failStep
		echo "ERROR: Could not remove all xml files!"
		echo "       Check error log under $log."
		exit 1
	else
		tickStep
	fi
addStep "Converting graphics..."
	failed=0
	convert $rp/$plugin/icon.png -resize 256x256 $rp/$plugin/icon.png 2>&1 >> $log
	if [[ $? != 0 ]]; then
		failed=1
	fi
	if [[ $failed != 0 ]]; then
		failStep
		echo "ERROR: Could not convert all image files!"
		echo "       Check error log under $log."
		exit 1
	else
		tickStep
	fi
	
echo "Finalizing build..."
addStep "Creating zip package..."
	failed=0
	cd $rp 2>&1 >> $log
	zip -r $plugin.zip ./* 2>&1 >> $log
		if [[ $? != 0 ]]; then
		failed=1
	fi
		if [[ $failed != 0 ]]; then
		failStep
		echo "ERROR: Could not create zip package!"
		echo "       Check error log under $log."
		exit 1
	else
		tickStep
	fi
echo "Build complete!"
exit 0
