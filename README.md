# Important note
This plugin ~~has~~ *should have* been **discontinued** and replaced by the more generic "Danbooru" plugin available here: https://bitbucket.org/C_Classic/kodi.plugin.image.danbooru

After the release of the new *Danbooru* Plugin (~~Scheduled to ship with Kodi 14~~), this plugin will be removed from the official XBMC repositories. The new plugin will support all features of this one and much more. There are no downsides to migrating once it's available.

# Moebooru-Client for XBMC Version 0.95 Beta #
*Copyright (C) 2016  Markus Koch <CClassicVideos@aol.com>*

This project allows you to access [Moebooru](https://bitbucket.org/edogawaconan/moebooru)-based image boards from within your [Kodi](https://kodi.tv/)-Installation.

## Features ##
* Browse the latest images
* Search for images using tags (with search history)
* Search for tags
* Slideshows and favorites using the built-in functions of Kodi
* Browse pools

## How to install ##
To get the latest stable release, simply install the add-on from the [official repository](http://wiki.xbmc.org/?title=Add-ons). To do this, open the *Pictures*-tab in XBMC, navigate to *Add-ons* -> *Get More...*. Now select *Moebooru* and choose *Install*. You can now use the add-on from the *Picture Add-Ons* page. Alternatively, you can download the latest stable release from the [downloads-tab](https://bitbucket.org/C_Classic/xbmc.plugin.image.moebooru/downloads) as a zip-package and install the add-on that way.

In case you want to use the cutting edge development version of this program, you have to clone this git-repository. If you don't have git installed, you can download a zip-version [here](https://bitbucket.org/C_Classic/xbmc.plugin.image.moebooru/get/master.zip). After the download finished, copy the *plugin.image.moebooru*-folder into XBMC's [add-ons directory](http://wiki.xbmc.org/?title=Userdata#Location_of_the_UserData_folder).

## Settings ##
### Server settings ###
* **Server-address**: Specifies the image board to use. (default: http://konachan.com)
* **Entries per page**: Specifies how many images to load in one go. (default: 25; max: 100)
### Search settings ###
* **Default order**: Specifies how to sort the search results. (default: Server default)
* **Rating**: Allows you to select a specific category for the search results. (default: Questionable and Safe)
* **Sort tags by**: Specifies how to sort the tag search results. (default: Server default)
### Miscellaneous ###
* **Load preview for pools**: Specifies whether preview images shall be displayed in the list of pools. (default: Off)
* **Load preview for tags**: Specifies whether preview images shall be displayed in the list of tags. (default: On)
* **Size of the search history**: Defines how many entries to keep in the search history. (default: 10, max: 100)


## License ##
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation.
This program is distributed in the hope that it will be useful, but **without any warranty**; without even the implied warranty of **merchantability** or **fitness for a particular purpose**.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).


## Version history ##
### 0.95 Beta ###
	2016-12-27	Quickfix to adapt to konachan API changes
### 0.9 Beta ###
    2013-07-30  Bugfixes
    2013-10-29	Changed language IDs to fulfill the official requirements
### 0.8 Beta ###
    2013-06-20  Added sorting options for tags
                Various bugfixes
### 0.7 Beta ###
    2013-06-15  Added related tags for tags
    2013-06-16  Added the search for tags
                Search will be automatically invoked if no images were retrieved
### 0.6 Beta ###
    2013-06-04  Fixed random
                Added installation instructions
    2013-06-14  Added a function to show the tags of the image to the context menu
    2013-06-15  Added preview images for related tags
### 0.5 Beta ###
    2013-05-29  Fixed an error requiring the user to reenter the search query when using the slideshow button
### 0.4 Beta ###
    2013-05-20  Updated translation system
                Various bugfixes
                Added the German translation
### 0.3 Beta ###
    2013-05-19  Various bugfixes and optimizations
    2013-05-20  Added proper icons
                Added a fanart
### 0.2 Beta ###
    2013-04-01  Added default search parameters (rating and order)
    2013-04-03  Finished the pools
                Now using XBMC's built-in caching function
                Spring cleaning
    2013-04-18  Added random
    2013-04-26  Added preview images for pools and the search history
### 0.1 Beta ###
    2013-03-29  Started development
    2013-03-31  Finished latest and basic search
    2013-04-01  Started with the pools
